﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebRestApi.Models;

namespace WebRestApi.DbBuilds
{
    public class ManageStatus
    {
        // fields
        private const string _connectionString = "Server=tcp:myfoodjournal-server.database.windows.net,1433;" +
                                                 "Initial Catalog = MyFoodJournalDB;Persist Security Info=False;" +
                                                 "User ID = AdminFood;" +
                                                 "Password=MyFoodApp#1234;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout = 30;";
        private const string GET_ALL = "select * from Tam.Status";
        private const string GET_ONE = "select * from Tam.Status WHERE Id = @ID";
        private const string POST_ONE = "insert into Tam.Status (Mood, Food, Hp, FkUserId) VALUES (@MOOD, @FOOD, @HP, @FKUSERID)";

        private const string PUT_ONE =
            "UPDATE Tam.Status SET Mood = @MOOD, Food = @FOOD, Hp = @HP, FkUserId = @FKUSERID WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM Tam.Status WHERE Id = @ID";

        // prop
        public IEnumerable<Status> StatusList { get; set; }


        public IEnumerable<Status> Get()
        {
            return StatusList;
        }

        /// <summary>
        /// reads the data from the reader and returns a Status obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the Status obj from the reader</returns>
        protected Status ReadNextElement(SqlDataReader reader)
        {
            Status status = new Status();

            status.Id = reader.GetInt32(0);
            status.Mood = reader.GetInt32(1);
            status.Food = reader.GetInt32(2);
            status.Hp = reader.GetInt32(3);
            status.FkUserId = reader.GetInt32(4);
            status.When = reader.GetDateTime(5);

            return status;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all Status</returns>
        public IEnumerable<Status> LoadStatusList()
        {
            List<Status> tempStatusList = new List<Status>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Status value = ReadNextElement(reader);
                    tempStatusList.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempStatusList;
        }

        public Status Get(int id)
        {
            Status tempStatus = new Status();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Status value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempStatus = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempStatus;
        }

        public void Post(Status value)
        {
            Status tempStatus = new Status();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@MOOD", value.Mood);
                cmd.Parameters.AddWithValue("@FOOD", value.Food);
                cmd.Parameters.AddWithValue("@HP", value.Hp);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempStatus = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                StatusList = LoadStatusList();
            }
        }

        public void Put(int id, Status value)
        {
            Status tempStatus = new Status();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@MOOD", value.Mood);
                cmd.Parameters.AddWithValue("@FOOD", value.Food);
                cmd.Parameters.AddWithValue("@HP", value.Hp);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);
                cmd.Parameters.AddWithValue("@ID", id);


                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempStatus = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                StatusList = LoadStatusList();
            }
        }

        public void Delete(int id)
        {
            Status tempStatus = new Status();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempStatus = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                StatusList = LoadStatusList();
            }
        }
    }
}
