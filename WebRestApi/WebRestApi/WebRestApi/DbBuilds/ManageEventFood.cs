﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebRestApi.Models;

namespace WebRestApi.DbBuilds
{
    public class ManageEventFood
    {
        // fields
        private const string _connectionString = "Server=tcp:myfoodjournal-server.database.windows.net,1433;" +
                                                 "Initial Catalog = MyFoodJournalDB;Persist Security Info=False;" +
                                                 "User ID = AdminFood;" +
                                                 "Password=MyFoodApp#1234;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout = 30;";
        private const string GET_ALL = "select * from Tam.EventFood";
        private const string GET_ONE = "select * from Tam.EventFood WHERE Id = @ID";
        private const string POST_ONE = "insert into Tam.EventFood (Text, FkUserId) VALUES (@TEXT, @FKUSERID)";

        private const string PUT_ONE =
            "UPDATE Tam.EventFood SET Text = @TEXT, FkUserId = @FKUSERID WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM Tam.EventFood WHERE Id = @ID";


        // prop
        public IEnumerable<EventFood> FoodEvents { get; set; }


        public IEnumerable<EventFood> Get()
        {
            return FoodEvents;
        }

        /// <summary>
        /// reads the data from the reader and returns a EventFood obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the EventFood obj from the reader</returns>
        protected EventFood ReadNextElement(SqlDataReader reader)
        {
            EventFood food = new EventFood();

            food.Id = reader.GetInt32(0);
            food.Text = reader.GetString(1);
            food.When = reader.GetDateTime(2);
            food.FkUserId = reader.GetInt32(3);

            return food;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all EventFood</returns>
        public IEnumerable<EventFood> LoadFoodEvents()
        {
            List<EventFood> tempFoods = new List<EventFood>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventFood value = ReadNextElement(reader);
                    tempFoods.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempFoods;
        }

        public EventFood Get(int id)
        {
            EventFood tempFood = new EventFood();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    EventFood value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempFood = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempFood;
        }

        public void Post(EventFood value)
        {
            EventFood tempFood = new EventFood();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempFood = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                FoodEvents = LoadFoodEvents();
            }
        }

        public void Put(int id, EventFood value)
        {
            EventFood tempFood = new EventFood();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@TEXT", value.Text);
                cmd.Parameters.AddWithValue("@FKUSERID", value.FkUserId);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempFood = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                FoodEvents = LoadFoodEvents();
            }
        }

        public void Delete(int id)
        {
            EventFood tempFood = new EventFood();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(DELETE_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempFood = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                FoodEvents = LoadFoodEvents();
            }
        }
    }
}
