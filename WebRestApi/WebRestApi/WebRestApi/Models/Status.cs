﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRestApi.Models
{
    public class Status
    {
        public int Id { get; set; }
        public int Mood { get; set; }
        public int Food { get; set; }
        public int Hp { get; set; }
        public DateTime When { get; set; }
        public int FkUserId { get; set; }

        public Status()
        {
            
        }

        public Status(int id, int mood, int food, int hp, DateTime @when, int fkUserId)
        {
            Id = id;
            Mood = mood;
            Food = food;
            Hp = hp;
            When = when;
            FkUserId = fkUserId;
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Mood)}: {Mood}, {nameof(Food)}: {Food}, {nameof(Hp)}: {Hp}, {nameof(When)}: {When}, {nameof(FkUserId)}: {FkUserId}";
        }
    }
}
