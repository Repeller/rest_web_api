﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebRestApi.Models
{
    public class EventFood
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime When { get; set; }
        public int FkUserId { get; set; }

        public EventFood()
        {
            
        }

        public EventFood(int id, string text, DateTime @when, int fkUserId)
        {
            Id = id;
            Text = text;
            When = when;
            FkUserId = fkUserId;
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Text)}: {Text}, {nameof(When)}: {When}, {nameof(FkUserId)}: {FkUserId}";
        }
    }
}
