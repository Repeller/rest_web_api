﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebRestApi.DbBuilds;
using WebRestApi.Models;

namespace WebRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        public ManageStatus ManageEvents = new ManageStatus();

        public StatusController()
        {
            ManageEvents.StatusList = ManageEvents.LoadStatusList();
        }

        /// <summary>
        /// gets all the StatusEvents
        /// </summary>
        /// <returns>the list of all StatusEvents</returns>
        [HttpGet]
        public IEnumerable<Status> Get()
        {
            return ManageEvents.StatusList;
        }

        // GET: api/Status/5
        //[HttpGet("{id}", Name = "Get")]
        /// <summary>
        /// get one Status by id
        /// </summary>
        /// <param name="id">the id of the Status you want</param>
        /// <returns>the Status you are looking for</returns>
        [HttpGet]
        [Route("{id}")]
        public Status Get(int id)
        {
            //Item error = new Item(0, "error", "error", 404.404);

            return ManageEvents.Get(id);
        }

        // POST: api/Status
        /// <summary>
        /// post one Status to the list
        /// </summary>
        /// <param name="value">the Status you want to add to the list</param>
        [HttpPost]
        public void Post([FromBody] Status value)
        {
            ManageEvents.Post(value);
        }

        // PUT: api/Status/5
        //[HttpPut("{id}")]
        /// <summary>
        /// put/edit one Status's values and replace them with new ones
        /// </summary>
        /// <param name="id">the id of the Status to put into</param>
        /// <param name="value">the value that will be put into the Status</param>
        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] Status value)
        {
            ManageEvents.Put(id, value);
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// delete one Status from the list
        /// </summary>
        /// <param name="id">the id of the Status you want to delete</param>
        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            ManageEvents.Delete(id);
        }
    }
}
