﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebRestApi.DbBuilds;
using WebRestApi.Models;

namespace WebRestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public ManageUser ManageUser = new ManageUser();

        public UserController()
        {
            ManageUser.Users = ManageUser.LoadUsers();
        }

        /// <summary>
        /// gets all the Users
        /// </summary>
        /// <returns>the list of all Users</returns>
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return ManageUser.Users;
        }

        // GET: api/Users/5
        //[HttpGet("{id}", Name = "Get")]
        /// <summary>
        /// get one Status by id
        /// </summary>
        /// <param name="id">the id of the User you want</param>
        /// <returns>the User you are looking for</returns>
        [HttpGet]
        [Route("{id}")]
        public User Get(int id)
        {
            //Item error = new Item(0, "error", "error", 404.404);

            return ManageUser.Get(id);
        }

        // POST: api/User
        /// <summary>
        /// post one User to the list
        /// </summary>
        /// <param name="value">the User you want to add to the list</param>
        [HttpPost]
        public void Post([FromBody] User value)
        {
            ManageUser.Post(value);
        }

        // PUT: api/User/5
        //[HttpPut("{id}")]
        /// <summary>
        /// put/edit one User's values and replace them with new ones
        /// </summary>
        /// <param name="id">the id of the User to put into</param>
        /// <param name="value">the value that will be put into the User</param>
        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] User value)
        {
            ManageUser.Put(id, value);
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// delete one User from the list
        /// </summary>
        /// <param name="id">the id of the User you want to delete</param>
        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            ManageUser.Delete(id);
        }
    }
}
